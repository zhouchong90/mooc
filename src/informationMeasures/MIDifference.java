package informationMeasures;

import informationMeasures.MutualInformation.AbstractFeature;

public class MIDifference extends DiscriminatingFeatureSet{
	
	public static void main(String[] args) {
		MIDifference j = new MIDifference();

		long startTime = System.currentTimeMillis();
		j.calculateInfoloss();
		
		j.mergedFeatures();

		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		
		System.out.println("done in " + totalTime/1000);

	}
	
	public MIDifference(){
		super();
	}
	
	
	public double informationLoss(AbstractFeature feature1, AbstractFeature feature2){
		
		//calculate MI of merged feature
		double mergedMI = 0;
		
		double probTerm = feature1.featureProbability+feature2.featureProbability;
		for(int i = 0; i<w.documentSize; i++)
		{
			double prob = abstractFeatureProb(feature1,feature2,i);
			double probTC = (double)w.docSums[i]/w.matrixSum;
			mergedMI += prob*w.log2( prob/(probTerm*probTC) );
		}
		
		double MI1 = feature1.featureMIWithTC;
		double MI2 = feature2.featureMIWithTC;
		return (MI1+MI2-mergedMI);
	}

	public double abstractFeatureProb(AbstractFeature feature1, AbstractFeature feature2, int tcIndex)
	{
		return (double)(feature1.featureTotalCount[tcIndex]+feature2.featureTotalCount[tcIndex])/w.matrixSum;
	}
	
}
