package informationMeasures;

import informationMeasures.MutualInformation.AbstractFeature;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MustLinkMI {

	MutualInformation m;
	ArrayList<AbstractFeature> candidates = new ArrayList<AbstractFeature>();

	public static void main(String[] args) throws IOException {

		MustLinkMI w = new MustLinkMI();

		w.getCandidates();
		w.calculateMI();

	}

	private void getCandidates() {

		HashMap<AbstractFeature, Double> allscores = new HashMap<AbstractFeature, Double>();

		for (int i = 0; i < m.featureList.size(); i++) {
			double MI = (double) m.featureList.get(i).featureMIWithTC;
			;

			allscores.put(m.featureList.get(i), MI);
		}

		List<Map.Entry<AbstractFeature, Double>> list = new LinkedList<Map.Entry<AbstractFeature, Double>>(
				allscores.entrySet());
		
		Collections.sort(list,
				new Comparator<Map.Entry<AbstractFeature, Double>>() {
					public int compare(Map.Entry<AbstractFeature, Double> o1,
							Map.Entry<AbstractFeature, Double> o2) {
						return (o2.getValue()).compareTo(o1.getValue());
					}
				});
		
		for(int k = 0; k<50; k++)
		{
			candidates.add(list.get(k).getKey());
		}

	}

	public MustLinkMI() {
		m = new MutualInformation();
	}

	public void calculateMI() throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(
				"C:/Users/Chong/Desktop/MustLinkMIMerge.csv"));

//		for (int k = 0; k < 50; k++) {
			double minMI = Integer.MAX_VALUE;
			int maxI = 0;
			int maxJ = 1;

			for (int i = 0; i < candidates.size(); i++) {
				for (int j = i + 1; j < candidates.size(); j++) {
					double MI = informationLoss(candidates.get(i),
							candidates.get(j));

					if (MI < minMI) {
						minMI = MI;
						maxI = i;
						maxJ = j;
					}
					 m.printFeatureMerge(bw,m.featureList.get(i),m.featureList.get(j),MI);
				}
			}

			m.printFeatureMerge(bw, m.featureList.get(maxI),
					m.featureList.get(maxJ), minMI);
			bw.flush();

			AbstractFeature mergedFeature = m.new AbstractFeature(
					m.featureList.get(maxI), m.featureList.get(maxJ));

			// has to delete J first since J is larger than I.
			m.featureList.remove(maxJ);
			m.featureList.remove(maxI);
			m.featureList.add(mergedFeature);
//		}
	}

	
	public double informationLoss(AbstractFeature feature1, AbstractFeature feature2){
		//calculate informationLoss of the two feature if merged.
		
		//p(a)
		double featureProb = feature1.featureProbability+feature2.featureProbability;
		
		//H(term)
		ArrayList<Double> term = new ArrayList<Double> ();
		for(int i = 0; i < m.documentSize; i++)
		{
			double sum = 0;
			sum += (double)feature1.featureTotalCount[i]/m.matrixSum;
			sum += (double)feature2.featureTotalCount[i]/m.matrixSum;
			sum *= (double)1/featureProb;
			term.add(sum);
		}
		double entropy = entropy(term);
		
		//weight1 and H(term1)
		double weight1 = feature1.featureProbability/featureProb;
		ArrayList<Double> term1 = new ArrayList<Double> ();
		for(int i = 0; i < m.documentSize; i++)
		{
			double jointProb = (double)feature1.featureTotalCount[i]/m.matrixSum;
			term1.add(jointProb/feature1.featureProbability);
		}
		double entropy1 = entropy(term1);
		
		//weight2 and H(term2)
		double weight2 = feature2.featureProbability/featureProb;
		ArrayList<Double> term2 = new ArrayList<Double> ();
		for(int i = 0; i < m.documentSize; i++)
		{
			double jointProb = (double)feature2.featureTotalCount[i]/m.matrixSum;
			term2.add(jointProb/feature2.featureProbability);
		}
		double entropy2 = entropy(term2);
		
		double infoLoss = featureProb * (entropy - weight1*entropy1 - weight2*entropy2);
		
		return infoLoss;
	}
	
	public double entropy(ArrayList<Double> values)
	{
		double probSum = 0;
		
		for(double v:values)
		{
			probSum += v;
		}
		
		double result = 0;
		for(double v:values)
		{
			double prob = v/probSum;
			result -= prob*m.log2(prob);
		}
		 
		return result;
	}

}
