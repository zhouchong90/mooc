package informationMeasures;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class BinaryMutualInformation {
	
	
	private MutualInformation w = new MutualInformation();
	
	public static void main(String[] args) throws IOException {
		DiscriminatingFeatureSet j = new JSMergeLoss();

		long startTime = System.currentTimeMillis();
    	
		//pair wise info. loss
		j.calculateInfoloss();
		
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		
		System.out.println("done in " + totalTime/1000);
	}
	
	private void calculateInfoloss() throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/BinaryMI.csv"));
		
		bw.append("word");
		bw.append(",");
		bw.append("highestMI");
		bw.append(",");
		bw.append("highestMI-secondHighest");
		bw.append(",");
		bw.append("highest/8+highest-secondHighest");
		bw.append("\n");
		
		for(int i = 0; i< w.termSums.length; i++)
		{
			ArrayList<Double> MIScores = new ArrayList<Double>();
			
			for(int j = 0; j < w.documentSize; j++)
			{
				MIScores.add(MIScore(i, j));
			}
						
			Collections.sort(MIScores);
			bw.append(w.inverseDictionary.get(i));
			bw.append(",");
			double highest = MIScores.get(MIScores.size()-1);
			double secondHigh =  MIScores.get(MIScores.size()-2);
			bw.append(String.valueOf(highest));
			bw.append(",");
			bw.append(String.valueOf(highest-secondHigh));
			bw.append(",");
			bw.append(String.valueOf(highest/8+highest-secondHigh));
			bw.append("\n");
		}
		bw.close();
	}
	
	private double MIScore(int termIndex, int docIndex){
		//calculate information
		
		double termP = (double)w.termSums[termIndex]/w.matrixSum;
		double notTermP = 1-termP;
		double docP = (double)w.docSums[docIndex]/w.matrixSum;
		double notDocP = 1-docP;
		
		double mi = 0;
		
		//term1
		double p1 = (double)w.termDocMat[termIndex][docIndex]/w.matrixSum;		
		mi += p1*w.log2( p1/(termP*docP) );
		
		//term2
		double p2 = (double)(w.termSums[termIndex] - w.termDocMat[termIndex][docIndex])/w.matrixSum;
		mi += p2*w.log2( p2/(termP*notDocP) );
		
		//term3
		double p3 = (double)(w.docSums[docIndex] - w.termDocMat[termIndex][docIndex])/w.matrixSum;
		mi += p3*w.log2( p3/(notTermP*docP) );
		
		//term4
		double p4 = 1-p1;
		mi += p4*w.log2( p4/(notTermP*notDocP) );
		
		return mi;
	}
	
}
