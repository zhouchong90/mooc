package informationMeasures;

import informationMeasures.MutualInformation.AbstractFeature;

public class ConditionMI extends DiscriminatingFeatureSet {

	public static void main(String[] args) {
		
		ConditionMI j = new ConditionMI();

		long startTime = System.currentTimeMillis();
		j.calculateInfoloss();
		
		j.mergedFeatures();

		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		
		System.out.println("done in " + totalTime/1000);
	}
	
	public ConditionMI(){
		super();
	}

	@Override
	//MI(x,y|tc)
	double informationLoss(AbstractFeature a1, AbstractFeature a2) {
		
		double MI = 0;
		
		for (int i = 0; i<w.documentSize; i++){
			double inside = 1;
			
			double pxyz = (double)Math.min(a1.featureTotalCount[i], a2.featureTotalCount[i])/w.matrixSum;
			inside *= pxyz;
			
			double pz = (double)w.docSums[i]/w.matrixSum;
			inside *= pz;
			
			double pxz = (double)a1.featureTotalCount[i]/w.matrixSum;
			double pyz = (double)a2.featureTotalCount[i]/w.matrixSum;
			
			if(pxyz != 0 )
			{
				inside /= pxz;
				inside /= pyz;
			}
			
			MI += pxyz==0 ? 0 : pxyz*w.log2(inside);
		}
		
		return MI;
	}
	
}
