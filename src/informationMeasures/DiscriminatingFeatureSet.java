package informationMeasures;

import informationMeasures.MutualInformation.AbstractFeature;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public abstract class DiscriminatingFeatureSet {

	MutualInformation w;
	
	public DiscriminatingFeatureSet() {
		w = new MutualInformation();
	}

	public void mergedFeatures(){
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/MILossMerge.csv"));
			
			for(int k = 0; k<50; k++)
			{	
				double maxMI = 0;
				int maxI = 0;
				int maxJ = 1;
				
				//find the max MI loss and their index
				for(int i = 0; i< w.featureList.size()-1; i++)
				{
					for(int j = i+1; j< w.featureList.size(); j++)
					{
						double MI = informationLoss(w.featureList.get(i),w.featureList.get(j));
						if(MI>maxMI)
						{
							maxMI = MI;
							maxI = i;
							maxJ = j;
						}
					}
				}
				//merge feature I and J
				AbstractFeature mergedFeature = w.new AbstractFeature(w.featureList.get(maxI),w.featureList.get(maxJ));
				w.printFeatureMerge(bw,w.featureList.get(maxI),w.featureList.get(maxJ),maxMI);
				//has to delete J first since J is larger than I.\
				//TODO: for overlapping sets
				w.featureList.remove(maxJ);
				w.featureList.remove(maxI);			
				w.featureList.add(mergedFeature);
			}
			bw.close();
			
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void mergedFeaturesOverlapping(){
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/MILossMerge.csv"));
			
			ArrayList<HashSet<Integer>> DFS = new ArrayList<HashSet<Integer>>();
			
			for(int k = 0; k<50; k++)
			{	
				double maxMI = 0;
				int maxI = 0;
				int maxJ = 1;
				
				//find the max MI loss and their index
				for(int i = 0; i< w.featureList.size()-1; i++)
				{
					for(int j = i+1; j< w.featureList.size(); j++)
					{
						HashSet<Integer> proposedMerge = new HashSet<Integer>();
						proposedMerge.addAll(w.featureList.get(i).getFeatureIndexList());
						proposedMerge.addAll(w.featureList.get(j).getFeatureIndexList());
						
						boolean skip = false;
						for(HashSet<Integer> dfs:DFS)
						{
							if (belongs(proposedMerge,dfs))
							{
								skip = true;
							}
						}
						
						if(skip == true)
							continue;
						
						double MI = informationLoss(w.featureList.get(i),w.featureList.get(j));
						if(MI>maxMI)
						{
							maxMI = MI;
							maxI = i;
							maxJ = j;
						}
					}
				}
				//merge feature I and J
				AbstractFeature mergedFeature = w.new AbstractFeature(w.featureList.get(maxI),w.featureList.get(maxJ));
				
				//has to delete J first since J is larger than I.\
				//TODO: for overlapping sets
				//w.featureList.remove(maxJ);
				//w.featureList.remove(maxI);
				HashSet<Integer> newdfs = new HashSet<Integer>();
				newdfs.addAll(mergedFeature.getFeatureIndexList());
				boolean contain = false;
				for(HashSet<Integer> dfs:DFS)
				{
					if(belongs(dfs,newdfs))
					{
						DFS.remove(dfs);
						DFS.add(newdfs);
						contain = true;
						w.printFeatureMerge(bw,w.featureList.get(maxI),w.featureList.get(maxJ),maxMI);
						bw.flush();
						break;
					}
				}
				if(!contain)
				{
					DFS.add(newdfs);
					w.printFeatureMerge(bw,w.featureList.get(maxI),w.featureList.get(maxJ),maxMI);
					bw.flush();
				}
				
				w.featureList.add(mergedFeature);
			}
			bw.close();
			
			BufferedWriter bw1 = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/DFS.csv"));
			for(HashSet<Integer> dfs:DFS)
			{
				for(int i:dfs)
				{
					bw1.append(w.inverseDictionary.get(i));
					bw1.append(",");
				}
				bw1.newLine();
			}
			
			bw1.close();
			
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	//return if a1 belongs to a2
	public boolean belongs(HashSet<Integer> a1, HashSet<Integer> a2)
	{
		for(int i:a1)
		{
			if(!a2.contains(i))
				return false;
		}
		return true;
	}
	
	public void calculateInfoloss(){
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/InfoLossResult.csv"));
			
			for(int i = 0; i< w.featureList.size(); i++)
			{
				for(int j = i+1; j< w.featureList.size(); j++)
				{
					double MI = informationLoss(w.featureList.get(i),w.featureList.get(j));
					
					w.printFeatureMerge(bw,w.featureList.get(i),w.featureList.get(j),MI);
				}
			}
			bw.close();
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	abstract double informationLoss(AbstractFeature abstractFeature,AbstractFeature abstractFeature2);

}