package informationMeasures;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class MutualInformation {

	final int minWordLength = 2;

	public HashSet<String> stopList = new HashSet<String>();

	/**
	 * term-index, index-term dictionary
	 */
	public HashMap<String, Integer> dictionary = new HashMap<String, Integer>();
	public HashMap<Integer, String> inverseDictionary = new HashMap<Integer, String>();

	public String fileFolder = "C:/Users/Chong/Google Drive/MOOC TM/data/Breast Cancer Forum/domain_lemmatized/";

	// term-document frequency matrix
	public int[][] termDocMat;

	public int documentSize = 0;
	
	public ArrayList<AbstractFeature> featureList;
	
	// sum of matrix
	int matrixSum = 0;
	int[] termSums;
	int[] docSums;
	
	public MutualInformation()
	{
		readInStopList();
		processDictionary();
		init();
		readInData();
		calSum();
		constructFeatureList();
	}
	
	
	public void printFeatureMerge(BufferedWriter bw, AbstractFeature af1, double MI) throws IOException
	{	
		for(int wordIndex : af1.featureIndexList)
		{
			bw.append(inverseDictionary.get(wordIndex));
			if(af1.featureIndexList.size()>1)
				bw.append("; ");
		}
		
		bw.append(",");
		
		bw.append(String.valueOf(MI));
		bw.newLine();
	}
	
	public void printFeatureMerge(BufferedWriter bw, AbstractFeature af1, AbstractFeature af2, double MI) throws IOException
	{
		
		for(int wordIndex : af1.featureIndexList)
		{
			bw.append(inverseDictionary.get(wordIndex));
			if(af1.featureIndexList.size()>1)
				bw.append("; ");
		}
		
		bw.append(",");
		
		for(int wordIndex : af2.featureIndexList)
		{
			bw.append(inverseDictionary.get(wordIndex));
			if(af2.featureIndexList.size()>1)
			bw.append("; ");
		}
		
		bw.append(",");
		
		bw.append(String.valueOf(MI));
		bw.newLine();
	}
	
	public double log2(double a)
	{
		//if zero
		if (new Double(0.0).equals(a))
			return 0.0;
		return Math.log(a)/Math.log(2);
	}
	
	public void readInStopList() {
		try {
			BufferedReader br = new BufferedReader(
					new FileReader(
							"C:/Users/Chong/Google Drive/MOOC TM/code/data cleaning/stopWords.txt"));

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.isEmpty())
					stopList.add(line.trim());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void calSum() {
		for (int i = 0; i < termDocMat.length; i++)
			for (int j = 0; j < termDocMat[0].length; j++)
				matrixSum += termDocMat[i][j];

		// calculate the rowsum and colsum
		for (int i = 0; i < termDocMat.length; i++) {
			for (int j = 0; j < termDocMat[0].length; j++) {
				termSums[i] += termDocMat[i][j];
				docSums[j] += termDocMat[i][j];
			}
		}
	}

	public void readInData() {
		try {
			File fileFolder = new File(this.fileFolder);
			File[] listOfFiles = fileFolder.listFiles();

			int docIndex = 0;
			for (File doc : listOfFiles)
				if (doc.isFile() && doc.getName().split("\\.")[1].equals("txt")) {
					BufferedReader br = new BufferedReader(new FileReader(doc));

					String line;
					while ((line = br.readLine()) != null) {
						String[] words = line.split(" ");
						for (String word : words) {
							if (dictionary.containsKey(word))
								termDocMat[dictionary.get(word)][docIndex]++;
						}
					}
					br.close();
					docIndex++;
				}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void init() {
		termDocMat = new int[dictionary.size()][documentSize];
		// PMIMat = new double[dictionary.size()][documentSize];

		termSums = new int[termDocMat.length];
		docSums = new int[termDocMat[0].length];
		
		//manage all abstract features
		featureList = new ArrayList<AbstractFeature>();
	}

	public void constructFeatureList()
	{
		for(int index = 0; index < dictionary.size(); index++)
		{
			AbstractFeature f = new AbstractFeature(index);
			featureList.add(f);
		}
	}
	
	public void processDictionary() {
		try {
			File fileFolder = new File(this.fileFolder);
			File[] listOfFiles = fileFolder.listFiles();

			int index = 0;

			for (File doc : listOfFiles)
				if (doc.isFile() && doc.getName().split("\\.")[1].equals("txt")) {
					BufferedReader br = new BufferedReader(new FileReader(doc));

					String line;
					while ((line = br.readLine()) != null) {
						String[] words = line.split(" ");
						for (String word : words)
							if (word.length() >= minWordLength)
								if (!dictionary.containsKey(word)
										&& !stopList.contains(word)) {
									dictionary.put(word, index);
									inverseDictionary.put(index, word);
									index++;
								}
					}
					br.close();
					documentSize++;
				}
			System.out.println("dictionary size:" + dictionary.size());
			System.out.println("document size:" + documentSize);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public class AbstractFeature {
		private ArrayList<Integer> featureIndexList;
		
		public ArrayList<Integer> getFeatureIndexList() {
			return featureIndexList;
		}

		public double featureMIWithTC;
		public int featureTotalCount[];
		public double featureProbability;
		
		public AbstractFeature(int index){
			featureIndexList = new ArrayList<Integer>();
			featureIndexList.add(index);
			
			featureTotalCount = new int[documentSize];
			for(int i = 0; i<documentSize; i++)
			{
				featureTotalCount[i] = featureTotalCount(i);
			}
			featureProbability = featureProbability();
			featureMIWithTC = featureMIWithTC();
		}
		
		public AbstractFeature(AbstractFeature a1,AbstractFeature a2){
			featureIndexList = new ArrayList<Integer>();
			
			HashSet<Integer> set = new HashSet<Integer>();
			set.addAll(a1.featureIndexList);
			set.addAll(a2.featureIndexList);
			
			featureIndexList.addAll(set);
			
			featureTotalCount = new int[documentSize];
			for(int i = 0; i<documentSize; i++)
			{
				featureTotalCount[i] = featureTotalCount(i);
			}
			featureProbability = featureProbability();
			featureMIWithTC = featureMIWithTC();
		}
		
		//count all occurrence in this abstract feature
		private int featureTotalCount(int tcIndex)
		{
			int count = 0;
			for(int index:featureIndexList)
				count+=termDocMat[index][tcIndex];
			return count;
		}
		
		private double featureProbability(){
			int totalCount = 0;
			for(int index:featureIndexList)
				totalCount += termSums[index];
			return (double)totalCount/matrixSum;
		}
		
		private double featureMIWithTC(){
			double MI = 0;
			
			double probTerm = this.featureProbability();
			for(int i = 0; i<documentSize; i++)
			{
				double probJoint = (double)featureTotalCount[i]/matrixSum;	
				double probTC = (double)docSums[i]/matrixSum;
				MI += probJoint*log2( probJoint/(probTerm*probTC));
			}
			return MI;
		}
		
	}

}
