package preprocess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;

/**
 * delete all words that is not contained in the dictionary
 * @author Chong
 *
 */
public class TrimData
{

	public static void main(String[] args) throws IOException
	{
		//read dictionary
		HashSet<String> dict = new HashSet<String>();
		BufferedReader dictReader = new BufferedReader(new FileReader("C:/Users/Chong/Desktop/MOOC/dict"));
		
		String line;
		while((line=dictReader.readLine())!=null)
		{
			dict.add(line.trim());
		}
		dictReader.close();
		
		//for all files in the folder
		File fileFolder = new File("C:/Users/Chong/Desktop/MOOC/dataCleaned");
		File[] listOfFiles = fileFolder.listFiles();
		for(File file:listOfFiles)
		{
			if (file.isFile())
			{
				String fileName = file.getName().trim();
				BufferedReader br = new BufferedReader(new FileReader(file));
				BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/MOOC/dataTrimed/"+fileName));
				
				while((line = br.readLine())!=null)
				{
					String words[] = line.split(" ");
					for (String word:words)
					{
						if(dict.contains(word))
							bw.write(word+" ");
					}
					bw.newLine();
				}
				bw.close();
				br.close();
			}
		}

	}

}
