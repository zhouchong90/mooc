package preprocess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

public class ExportAsSVMLight {
	final int minWordLength = 2;
	public HashSet<String> stopList = new HashSet<String>();
	public HashMap<String, Integer> dictionary = new HashMap<String, Integer>();
	public HashMap<Integer, String> document = new HashMap<Integer, String>();
	public HashMap<Integer,String> trueClass = new HashMap<Integer,String>();
	
	
	public String fileFolder = "C:/Users/Chong/Google Drive/MOOC TM/data/Breast Cancer Forum/data_lemmatized_small/";
	
	public int documentSize = 0;
	
	public static void main(String[] args) throws IOException {
		
		ExportAsSVMLight e = new ExportAsSVMLight();
		
		e.readInStopList();
		//e.readInValidDocList();
		e.processDictionary();
		e.readInData();
		
		e.export();
		
	}

	public void readInStopList() {
		try {
			BufferedReader br = new BufferedReader(
					new FileReader(
							"C:/Users/Chong/Google Drive/MOOC TM/code/data cleaning/stopWords.txt"));

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.isEmpty())
					stopList.add(line.trim());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void readInValidDocList()
	{
		try {
			BufferedReader br = new BufferedReader(
					new FileReader(
							"C:/Users/Chong/Google Drive/MOOC TM/Syllabus TM paper/True Class.csv"));

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.isEmpty())
				{
					String[] s = line.split(",");
					if(!s[1].equals("0"))
						trueClass.put(Integer.parseInt(s[0].trim()), s[1].trim() );
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void processDictionary() {
		try {
			File fileFolder = new File(this.fileFolder);
			File[] listOfFiles = fileFolder.listFiles();

			//write dictionary
			BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/breast_dictionary.txt"));
			
			int index = 0;

			for (File doc : listOfFiles)
				if (doc.isFile() && doc.getName().split("\\.")[1].equals("txt")) {
					BufferedReader br = new BufferedReader(new FileReader(doc));

					String line;
					while ((line = br.readLine()) != null) {
						String[] words = line.split(" ");
						for (String word : words)
							if (word.length() >= minWordLength)
								if (!dictionary.containsKey(word) && !stopList.contains(word)) {
									dictionary.put(word, index);
									bw.write(word);
									bw.newLine();
									index++;
								}
					}
					br.close();
					documentSize++;
				}
			
			bw.close();
			System.out.println("dictionary size:" + dictionary.size());
			System.out.println("document size:" + documentSize);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readInData() {
		
		int documentCount = 0;
		int postCount = 0;
		int wordCount = 0;
		
		try {
			File fileFolder = new File(this.fileFolder);
			File[] listOfFiles = fileFolder.listFiles();
			
			for (File doc : listOfFiles){
				
				//&& trueClass.containsKey(Integer.parseInt(doc.getName().split("\\.")[0])
				if (doc.isFile() && doc.getName().split("\\.")[1].equals("txt")) {
					
					documentCount++;
					BufferedReader br = new BufferedReader(new FileReader(doc));

					HashMap<Integer, Integer> docMap = new HashMap<Integer, Integer>();
					
					String line;
					while ((line = br.readLine()) != null) {
						postCount++;
						
						String[] words = line.split(" ");
						for (String word : words) {
							if (dictionary.containsKey(word)){
								wordCount++;
								int wordIndex = dictionary.get(word);
								if(docMap.containsKey(wordIndex))
									docMap.put(wordIndex, docMap.get(wordIndex)+1);
								else
									docMap.put(wordIndex, 1);
							
							}
						}
					}
					
					br.close();
					
					String docName = doc.getName().split("\\.")[0];
					
					StringBuffer sb = new StringBuffer();
					for(Integer i:docMap.keySet())
					{
						sb.append(i.toString());
						sb.append(":");
						sb.append(docMap.get(i).toString());
						sb.append(" ");
					}
					
					document.put(Integer.parseInt(docName), sb.toString());
				}
			}
			
			System.out.println("documentCount: "+documentCount);
			System.out.println("postCount: "+postCount);
			System.out.println("wordCount: "+wordCount);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void export() throws IOException {	
		
		//export trueclass and document simutaneously
		ArrayList<Integer> docIndices = new ArrayList<Integer>();
		docIndices.addAll(document.keySet());
		Collections.sort(docIndices);
		
		//write true class and svm light
//		BufferedWriter bw0 = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/true class.txt"));
		BufferedWriter bw1 = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/breast_data.docs"));
		
		for (int index:docIndices)
		{
//			bw0.append(String.valueOf(index));
//			bw0.append(",");
//			bw0.append(trueClass.get(index));
//			bw0.newLine();
			
			bw1.append(document.get(index));
			bw1.newLine();
		}
		
//		bw0.close();
		bw1.close();
	}
	
}
