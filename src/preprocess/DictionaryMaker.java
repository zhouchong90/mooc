package preprocess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

public class DictionaryMaker
{

	public static void main(String[] args) throws IOException
	{
		int minOccurrence = 3;
		
		
		//for all files in the folder
		File fileFolder = new File("C:/Users/Chong/Desktop/MOOC/dataCleaned");
		File[] listOfFiles = fileFolder.listFiles();

		HashMap<String, Integer> dict = new HashMap<String, Integer>();
		
		for(File file:listOfFiles)
		{
			if (file.isFile())
			{
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				while((line = br.readLine())!=null)  
				{
					String words[] = line.split(" ");
					for (String word:words)
					{
						if(dict.containsKey(word))
							dict.put(word, dict.get(word)+1);						
						else
							dict.put(word, 1);
					}
				}
				br.close();
			}
		}
		
		//read in the english standard dict
		HashSet<String> cmuDict = new HashSet<String>();
		BufferedReader br = new BufferedReader(new FileReader("C:/Users/Chong/Desktop/MOOC/cmudict.0.7a"));
		String line;
		while((line = br.readLine())!=null)
		{
			cmuDict.add( line.split(" ")[0].trim().toLowerCase() );
		}
		br.close();
		
		
		
		ArrayList<String> result = new ArrayList<String>();
		
		HashSet<String> resultSet = new HashSet<String>();
		resultSet.addAll(dict.keySet());
		resultSet.retainAll(cmuDict);
		
		for(String key:resultSet)
		{
			if(dict.get(key)>=minOccurrence)
				result.add(key);
		}
		
		Collections.sort(result);
		
		BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/MOOC/dict"));
		
		for (String word:result)
		{
			bw.write(word);
			bw.newLine();
		}
		
		bw.close();
		
	}

}
