package preprocess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

public class ExportOneDocPerLine {

	public String fileFolder = "C:/Users/Chong/Google Drive/MOOC TM/data/Breast Cancer Forum/data_lemmatized_small/";
	final int minWordLength = 2;
	public HashSet<String> stopList = new HashSet<String>();
	public HashMap<Integer, String> document = new HashMap<Integer, String>();
	public HashMap<Integer,String> trueClass = new HashMap<Integer,String>();
	public HashMap<String, Integer> dictionary = new HashMap<String, Integer>();
	
	public static void main(String[] args) {
		ExportOneDocPerLine e = new ExportOneDocPerLine();
		e.readInStopList();
		//e.readInValidDocList();
		e.processDictionary();
		e.readInData();
	}

	public void readInStopList() {
		try {
			BufferedReader br = new BufferedReader(
					new FileReader(
							"C:/Users/Chong/Google Drive/MOOC TM/code/data cleaning/stopWords.txt"));

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.isEmpty())
					stopList.add(line.trim());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void readInValidDocList()
	{
		try {
			BufferedReader br = new BufferedReader(
					new FileReader(
							"C:/Users/Chong/Google Drive/MOOC TM/Syllabus TM paper/True Class.csv"));

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.isEmpty())
				{
					String[] s = line.split(",");
					if(!s[1].equals("0"))
						trueClass.put(Integer.parseInt(s[0].trim()), s[1].trim() );
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}	
	
	public void processDictionary() {
		try {
			File fileFolder = new File(this.fileFolder);
			File[] listOfFiles = fileFolder.listFiles();

			//write dictionary
			BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/Mooc.vocab"));
			
			int index = 0;

			for (File doc : listOfFiles)
				
				//&& trueClass.containsKey(Integer.parseInt(doc.getName().split("\\.")[0]))
				if (doc.isFile() && doc.getName().split("\\.")[1].equals("txt")) {
					BufferedReader br = new BufferedReader(new FileReader(doc));

					String line;
					while ((line = br.readLine()) != null) {
						String[] words = line.split(" ");
						for (String word : words)
							if (word.length() >= minWordLength)
								if (!dictionary.containsKey(word) && !stopList.contains(word)) {
									dictionary.put(word, index);
									bw.write(String.valueOf(index));
									bw.write(":");
									bw.write(word);
									bw.newLine();
									index++;
								}
					}
					br.close();
				}
			
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readInData() {

		try {
			File fileFolder = new File(this.fileFolder);
			File[] listOfFiles = fileFolder.listFiles();

			BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/Mooc.docs"));
			
			for (File doc : listOfFiles){

				//&& trueClass.containsKey(Integer.parseInt(doc.getName().split("\\.")[0]))
				if (doc.isFile() && doc.getName().split("\\.")[1].equals("txt") ) {
								
					BufferedReader br = new BufferedReader(new FileReader(doc));
					StringBuffer sb = new StringBuffer();
					String line;
					while ((line = br.readLine()) != null) {
						String[] words = line.split(" ");
						for (String word : words) {
							if (dictionary.containsKey(word) && !stopList.contains(word)){
								sb.append(dictionary.get(word));
								sb.append(" ");
							}
						}
					}
					
					br.close();
					bw.append(sb.toString());
					bw.newLine();
					
				}
			}
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
