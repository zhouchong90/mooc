package preprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class JsonExtractor
{			
	public static void main(String[] args) throws IOException
	{	
		//for each thread, how many posts are there in a thread
		HashMap<String, Integer> threadPostDist = new HashMap<String, Integer>();
		
		//for finding which posts are in which week. an array list of 8 elements that stores the list of thread numbers.
		HashMap<String, Date> weekThreads = new HashMap<String, Date>();
		
		// for all files in the folder
		File fileFolder = new File(
				"C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/data_raw/");
		File[] listOfFiles = fileFolder.listFiles();

		HashMap<String, StringBuffer> documents = new HashMap<String, StringBuffer>();

		int j = 0;// total post count

		// for every document in the folder
		for (File file : listOfFiles)
		{
			if (file.isFile())
			{
				String name = file.getName().trim();
				String[] names = StringUtils.split(name, ".|_");
				String fileName = names[0];
				
				String fileContent = new String(Files.readAllBytes(Paths.get(file.getPath())),StandardCharsets.UTF_8);
				fileContent = StringUtils.stripAccents(fileContent);
				fileContent = fileContent.replaceAll("[^\\p{ASCII}]", " ");
				fileContent = fileContent.replaceAll("\n", " ");
				fileContent = fileContent.replaceAll("nbsp", " ");
				
				// parse the document
				try{
					JsonReader reader = new JsonReader(new StringReader(fileContent));
					reader.setLenient(true);
					JsonParser parser = new JsonParser();
					JsonObject document = parser.parse(reader)
							.getAsJsonObject();
	
					// extract post title:
					try{
						String title = document.get("title").getAsString();
						
						if (!documents.containsKey(fileName))//new thread
						{
							StringBuffer sb = new StringBuffer();
							sb.append(title);
							documents.put(fileName, sb);
							
							threadPostDist.put(fileName, 1);
							
							Date time = new Date(document.get("posted_time").getAsBigInteger().longValue()*1000);
							
							weekThreads.put(fileName, time);
						}
					}catch(Exception e)
					{
						System.err.println(e);
					}
	
					// extract posts
					try{
						JsonArray posts = document.getAsJsonArray("posts");
						for (int i = 0; i < posts.size(); i++)
						{
							JsonObject post = posts.get(i).getAsJsonObject();
							String text = post.get("post_text").getAsString();
							addToDocument(documents, fileName, text, threadPostDist);
							j++;
							//for initial post
							break;
						}
					}catch(Exception e)
					{
						//System.err.println(e);
					}
	
					// extract comment, for intial posts, comment out.
					/*
					try{
						JsonArray comments = document.getAsJsonArray("comments");
						for (int i = 0; i < comments.size(); i++)
						{
							JsonObject comment = comments.get(i).getAsJsonObject();
							String text = comment.get("comment_text").getAsString();
							addToDocument(documents, fileName, text, threadPostDist);
							j++;
						}
					}catch(Exception e)
					{
						//System.err.println(e);
					}*/
					
				}catch(Exception e)
				{
					System.err.println(file.getName() + " Fatal error, dumping this file");
					System.err.print(e);
				}
			}
		}

		//output documents
		int i = 0;// total document count
		int k = 0;// total document containing "?"
		for (String id : documents.keySet())
		{
			StringBuffer sb = documents.get(id);
			if (sb.length() > 6)
			{
				String outputDir = "C:/Users/Chong/Desktop/MOOC/initial_posts/";
				
				//create directory
				File f = new File(outputDir);
				if(!f.exists()||!f.isDirectory())
					Files.createDirectories(Paths.get(outputDir));
					
				String fileName = outputDir + id + ".txt";
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(fileName)));
				String desc = sb.toString();
				bw.write(desc);				
				bw.close();
				if (desc.contains("?"))
					k++;
				i++;
			}
		}
		System.out.println("total post and comments count: " + j);
		System.out.println("total thread count: " + i);
		System.out.println("total threads containing question mark: " + k);
		
		/*
		//output thread post length distribution
		String outputDir = "C:/Users/Chong/Desktop/MOOC/";
		String fileName = outputDir + "thread_post_length_dist.txt";
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(fileName)));
		for (Integer postNum :threadPostDist.values())
		{		
			bw.append(postNum.toString());
			bw.append("\n");
		}
		bw.close();
		*/
		
	/*
		//output dates
		String outputDir = "C:/Users/Chong/Desktop/MOOC/";
		String fileName = outputDir + "post_dates.csv";
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(fileName)));
		for (String postNum :weekThreads.keySet())
		{		
			bw.append(postNum.toString()+",");
			bw.append(weekThreads.get(postNum).toString()+"\n");
		}
		bw.newLine();
		bw.close();*/
	}


	private static void addToDocument(HashMap<String, StringBuffer> documents,
			String fileName, String text, HashMap<String, Integer> threadPostDist)
	{
		if (documents.containsKey(fileName))//old document
		{
			StringBuffer sb = documents.get(fileName);
			sb.append("\n");
			sb.append(text);
			
			threadPostDist.put(fileName, threadPostDist.get(fileName)+1);
		} else // new document
		{
			StringBuffer sb = new StringBuffer();
			sb.append(text);
			documents.put(fileName, sb);
			
			threadPostDist.put(fileName, 1);
		}
	}

}
