package preprocess;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import statAnalysis.DocumentSimilarityWithSyllabus.Document;

public class Lemmatizer {

	HashMap<String,String> lemma = new HashMap<String,String>();
	HashSet<String> dictionary = new HashSet<String>();
	HashSet<String> stopList = new HashSet<String>();
	
	public static void main(String[] args) {
		Lemmatizer l = new Lemmatizer();
		
		l.readLemma();
		l.readDictionary();
		l.readInStopList();
		
		l.LemmatizeData();
		System.out.println("done");
	}
	
	public void LemmatizeData()
	{
		try{
			File fileFolder = new File(
					"C:/Users/Chong/Desktop/domain_corpus");
			File[] listOfFiles = fileFolder.listFiles();
			
			String outputFolder = "C:/Users/Chong/Desktop/domain_lemmatized/";
			
			for (File doc:listOfFiles)
			{
				if (doc.isFile() && doc.getName().split("\\.")[1].equals("txt"))
				{
					//if(Math.random()>0.1)
					//	continue;
					
					String outputFileName = doc.getName();
					
					BufferedReader br= new BufferedReader(new FileReader(doc));
					BufferedWriter bw = new BufferedWriter(new FileWriter(outputFolder+outputFileName));
					
					String line;
					while((line = br.readLine())!=null)
					{
						String [] words = line.split(" ");
						for(String word:words)
						{
							
							if(lemma.containsKey(word))
							{
								if(!stopList.contains(lemma.get(word)))
								{
									bw.write(lemma.get(word));
									bw.write(" ");
								}
							}else
							{
								try{
									String shorten = word.split("'")[0];
									
									if(!shorten.isEmpty())
									{
										if(lemma.containsKey(shorten)){
											if(!stopList.contains(lemma.get(shorten)))
											{
												bw.write(lemma.get(shorten));
												bw.write(" ");
											}
										}
										else
										{
											if(dictionary.size()>0)
											{
												if(dictionary.contains(shorten))
													if(!stopList.contains(shorten))
													{
														bw.write(shorten);
														bw.write(" ");
													}
											}
											else{
												if(!stopList.contains(shorten))
												{
													bw.write(shorten);
													bw.write(" ");
												}
											}
										}
											
									}
								}catch(ArrayIndexOutOfBoundsException e)
								{
									System.out.println(word);
								}
							}
						}
						bw.newLine();
					}
					
					br.close();
					bw.close();
				}
			}
			
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
	
	public void readDictionary()
	{
		try {
			BufferedReader br = new BufferedReader(
					new FileReader(
							"C:/Users/Chong/Google Drive/MOOC TM/data/Breast Cancer Forum/breast_dictionary_small.txt"));

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.isEmpty())
					dictionary.add(line.trim());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readInStopList() {
		try {
			BufferedReader br = new BufferedReader(
					new FileReader(
							"C:/Users/Chong/Google Drive/MOOC TM/code/data cleaning/stopWords.txt"));

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.isEmpty())
					stopList.add(line.trim());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void readLemma()
	{
		try{
			
			FileReader fr = new FileReader("C:/Users/Chong/Google Drive/MOOC TM/code/data cleaning/my lemma list_no hypen_lower case.txt");
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine())!=null){
				String [] leftRight = line.split("->");
				String value = leftRight[0].trim();
				String [] keys = leftRight[1].trim().split(",");
				for(String key:keys)
					lemma.put(key.trim(), value);
			}
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}

}
