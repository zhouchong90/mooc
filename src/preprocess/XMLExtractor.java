package preprocess;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class XMLExtractor {

	HashMap<String, StringBuffer> documents = new HashMap<String, StringBuffer>();
	
	public static void main(String[] args) throws IOException {

		XMLExtractor x = new XMLExtractor();
		x.readDocs();
		x.writeDocs();
	}

	public void readDocs() throws IOException {
		int total = 0;
		int errors = 0;
		// for all files in the folder
		File fileFolder = new File(
				"C:/Users/Chong/Google Drive/MOOC TM/data/Breast Cancer Forum/data_raw/");
		File[] listOfFiles = fileFolder.listFiles();

		for (File file : listOfFiles) {
			String name = file.getName().trim();
			String[] names = StringUtils.split(name, ".");
			String fileName = names[0];
			if (file.isFile() && names[1].equals("xml")) {

				String fileContent = new String(Files.readAllBytes(Paths
						.get(file.getPath())), StandardCharsets.UTF_8);
				fileContent = StringUtils.stripAccents(fileContent);
				fileContent = fileContent.replaceAll("[^\\p{ASCII}]", " ");
				fileContent = fileContent.replaceAll("\n", " ");
				fileContent = fileContent.replaceAll("&nbsp;", " ");
				fileContent = fileContent.replaceAll("&apos;", "'");
				fileContent = fileContent.replaceAll("&#039;", "'");
				fileContent = fileContent.replaceAll("&hearts", " ");

				try {

					DocumentBuilderFactory dbf = DocumentBuilderFactory
							.newInstance();
					DocumentBuilder db = dbf.newDocumentBuilder();
					Document dom = db.parse(file);

					Element docEle = dom.getDocumentElement();

					StringBuffer sb = new StringBuffer();

					try{
						NodeList init = docEle.getElementsByTagName("InitPost");
						
						if (init != null && init.getLength() > 0) {
							for (int i = 0; i < init.getLength(); i++) {
								Element el = (Element) init.item(i);
	
								sb.append(el.getElementsByTagName("icontent").item(0).getFirstChild().getNodeValue());
								sb.append("\n");
							}
						}
					}catch(Exception e)
					{
						System.out.println("no matching init post tags");
					}

					try{
						NodeList posts = docEle.getElementsByTagName("Post");
						if (posts != null && posts.getLength() > 0) {
							for (int i = 0; i < posts.getLength(); i++) {
								Element el = (Element) posts.item(i);
	
								sb.append(el.getElementsByTagName("rcontent").item(0).getFirstChild().getNodeValue());
								sb.append("\n");
							}
						}
					}catch(Exception e)
					{
						System.out.println("no matching post tags");
					}

					if(sb.length()>0)
					{
						documents.put(fileName, sb);
						total++;
					}

				} catch (Exception e) {
					errors++;
				}
			}

		}
		
		System.out.println(total+","+errors);
	}

	public void writeDocs() throws IOException {
		// output documents

		for (String id : documents.keySet()) {
			StringBuffer sb = documents.get(id);
			if (sb.length() > 6) {
				String outputDir = "C:/Users/Chong/Desktop/breast/";

				// create directory
				File f = new File(outputDir);
				if (!f.exists() || !f.isDirectory())
					Files.createDirectories(Paths.get(outputDir));

				String fileName = outputDir + id + ".txt";
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(fileName)));
				String desc = sb.toString();
				bw.write(desc);
				bw.close();
			}
		}
	}

}
