package statAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

public class PairUpLDAResultsBySimilarity
{

	public static void main(String[] args) throws IOException
	{
		//read input
		FileReader fr1 = new FileReader("C:/Users/Chong/Google Drive/MOOC TM/result/Key Word Extraction/TopicCoherence/AMCResult.txt");
		FileReader fr2 = new FileReader("C:/Users/Chong/Google Drive/MOOC TM/result/Key Word Extraction/TopicCoherence/LDAResult.txt");

		BufferedReader br1 = new BufferedReader(fr1);
		BufferedReader br2 = new BufferedReader(fr2);
		
		LinkedList<String> docs1 = new LinkedList<String>();
		LinkedList<String> docs2 = new LinkedList<String>();
		
		readInDoc(br1,docs1);
		readInDoc(br2,docs2);
		
		if(docs1.size()!=docs2.size())
		{
			System.out.println("dimension not matched");
			return;
		}
		
		FileWriter fw = new FileWriter("C:/Users/Chong/Desktop/LDA_Compare.csv");
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write("number of similar tokens, All threads, Initial Posts\n");
		
		while (!docs1.isEmpty())
		{
			int maxSimilarity = 0;
			int bestMatchIndex1 = 0;
			int bestMatchIndex2 = 0;
			
			for(int i = 0; i<docs1.size(); i++)//while doc 1 still have something
			{			
				for(int j = 0; j < docs2.size(); j++)
				{
					int similarity = cosineSimilarity(docs1.get(i),docs2.get(j));
					if(similarity > maxSimilarity)
					{
						maxSimilarity = similarity;
						bestMatchIndex1 = i;
						bestMatchIndex2 = j;
					}
				}
			}
			String doc1 = docs1.remove(bestMatchIndex1);
			String doc2 = docs2.remove(bestMatchIndex2);
			printline(bw,maxSimilarity,doc1,doc2);
		}
		
		bw.close();
		bw.close();
		br1.close();
		br2.close();
		fr1.close();
		fr2.close();
		System.out.println("done");
	}
	
	public static int cosineSimilarity(String doc1, String doc2)
	{
		//since LDA results does not contain repeated words. 
		//use single count as vector product
		
		int similarTokens=0;
	
		HashSet<String> doc2Set = new HashSet<String>();
		
		String[] tokens2 = doc2.split(" ");
		for(String t2:tokens2)
		{
			doc2Set.add(t2);
		}
		
		String[] tokens1 = doc1.split(" ");
		for(String t1:tokens1)
		{
			if (doc2Set.contains(t1))
			{
				similarTokens++;
			}
		}
		
		System.out.print(tokens1.length);
		
		return similarTokens;		
	}
	
	public static void printline(BufferedWriter bw, int maxSimilarity, String line1, String line2) throws IOException
	{
		bw.write(new Integer(maxSimilarity).toString());
		bw.write(",");
		bw.write(line1);
		bw.write(",");
		bw.write(line2);
		bw.newLine();
	}
	
	public static void readInDoc(BufferedReader br, LinkedList<String> docs) throws IOException
	{
		String line;
		while ((line = br.readLine())!=null)
		{
			String doc = line.split(",")[2];
			docs.add(doc);
		}
	}
}
