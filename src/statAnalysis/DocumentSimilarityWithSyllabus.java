package statAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DocumentSimilarityWithSyllabus {

	public class Document{
		HashMap<String,Integer> termFrequency = new HashMap<String,Integer>();
		int length = 0;
		int documentID = 0;
	}
	
	ArrayList<Document> corpus = new ArrayList<Document>();
	ArrayList<HashMap<String, Integer>> syllabusTermFrequency = new ArrayList<HashMap<String, Integer>>();
	
	int [] syllabusSize = new int[8];
	
	//8*8 matrix, each row is a week's corpus, each column is a syllabus week
	double [][] similarity = new double[8][8];
	// size of the week's corpus
	int [] weekDocCount = new int[8];
	
	public static void main(String[] args) {
		
		System.out.println("Start");
		
		DocumentSimilarityWithSyllabus d = new DocumentSimilarityWithSyllabus();
		
		d.readInSyllabus();
		d.readInCorpus();
		
		d.computeSimilarity();
		d.normalize();
		d.printSimilarities();

		System.out.println("Done");
	}

	private void printSimilarities() {
		
		try{
			FileWriter fw = new FileWriter("C:/Users/Chong/Desktop/MOOC/DocSylSimilarity.csv");
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(int i = 0; i < 8; i++){
				StringBuffer sb = new StringBuffer();
				for(int j = 0; j < 8; j++){
					sb.append(similarity[i][j]);
					sb.append(",");
				}
				bw.write(sb.substring(0, sb.length()-1));
				bw.newLine();
			}
			
			bw.close();
			fw.close();
		}catch (IOException e){
			e.printStackTrace();
		}
		
	}

	private void addUpSimilarity(int rowIndex, double [] simArray)
	{
		for(int j = 0; j<8; j++)
			similarity[rowIndex][j]+=simArray[j];
	}
	
	private void normalize()
	{
		for(int i = 0; i<8; i++)
			for(int j = 0; j<8; j++)
				similarity[i][j]/=weekDocCount[i];
	}
	
	private void computeSimilarity() {
		
		double[] simArray = new double[8];
		for (Document doc:corpus)
		{	
			HashMap<String, Integer> docTerms = doc.termFrequency;
			
			for(int i = 0; i < syllabusTermFrequency.size(); i++)
			{
				int innerProduct = 0;
				HashMap<String, Integer> sylTerms = syllabusTermFrequency.get(i);
				for(String term:sylTerms.keySet())
				{
					if(docTerms.containsKey(term))
					{
						innerProduct+=docTerms.get(term)*sylTerms.get(term);
					}
						
				}
				simArray[i] = (double)innerProduct/(syllabusSize[i]*doc.length);
			}
			addUpSimilarity(weekIndex(doc.documentID),simArray);
		}
		
	}

	private void readInSyllabus() {
		try {
			String folderPath = "C:/Users/Chong/Desktop/MOOC/Syllabus/";

			for (int syllabusWeek = 1; syllabusWeek <= 8; syllabusWeek++) {
				
				FileReader fr = new FileReader(folderPath
						+ String.valueOf(syllabusWeek) + ".txt");
				BufferedReader br = new BufferedReader(fr);

				HashMap<String, Integer> document = new HashMap<String, Integer>();
				String line;
				
				while ((line = br.readLine()) != null) {
					
					if (!line.isEmpty()) {
						String[] words = line.split(" ");

						for (String word : words) {
							
							if (!word.isEmpty()){
								syllabusSize[syllabusWeek-1]++;
								safeAdd(document, word.toLowerCase());
							}
						}

					}
				}
				
				syllabusTermFrequency.add(document);
				
				br.close();
				fr.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void safeAdd(HashMap<String, Integer> document, String word) {
		if (document.containsKey(word)) {
			document.put(word, document.get(word) + 1);
		} else {
			document.put(word, 1);
		}
	}

	//return 0~7 as week index of the document
	private int weekIndex(int documentID)
	{
		if (documentID>=1 && documentID<=700) return 0;
		else if (documentID >=701 && documentID<=1149) return 1;
		else if (documentID >=1150 && documentID<=1468) return 2;
		else if (documentID >=1469 && documentID<=1833) return 3;
		else if (documentID >=1834 && documentID<=2234) return 4;
		else if (documentID >=2235 && documentID<=2526) return 5;	
		else if (documentID >=2527 && documentID<=2927) return 6;			
		else return 7;
	}
	
	private void readInCorpus(){
		try{
			File fileFolder = new File(
					"C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/initial_posts/");
			File[] listOfFiles = fileFolder.listFiles();
			
			for (File doc:listOfFiles)
			{
				if (doc.isFile() && doc.getName().split("\\.")[1].equals("txt"))
				{
					Document d = new Document();
					d.documentID = Integer.parseInt(doc.getName().split("\\.")[0]);
					
					weekDocCount[weekIndex(d.documentID)]++;
					
					FileReader fr = new FileReader(doc);
					BufferedReader br = new BufferedReader(fr);

					String line;
					
					while ((line = br.readLine()) != null) {
						
						if (!line.isEmpty()) {
							String[] words = line.split(" ");
							for (String word : words) {
								if (!word.isEmpty()) {
									safeAdd(d.termFrequency, word.toLowerCase());
									d.length++;
								}
							}
						}
					}
					
					corpus.add(d);
					
					br.close();
					fr.close();
				}
			}
			
			
		}catch(IOException e){
			e.printStackTrace();
		}
		
	}
}
