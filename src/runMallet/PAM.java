package runMallet;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import cc.mallet.topics.HierarchicalPAM;
import cc.mallet.types.InstanceList;
import cc.mallet.util.Randoms;


public class PAM
{
	public static void main(String[] args){
		
		String input1 = "C:/Users/Chong/Google Drive/MOOC TM/data/Machine Learning Forum/initial_posts.mallet";
		String output1 = "C:/Users/Chong/Google Drive/MOOC TM/result/PAM/Machine Learning Forum/PAM_init_2_20.txt";
		PipeRunner(input1, output1);

		String input2 = "C:/Users/Chong/Google Drive/MOOC TM/data/Machine Learning Forum/posts_comments.mallet";
		String output2 = "C:/Users/Chong/Google Drive/MOOC TM/result/PAM/Machine Learning Forum/PAM_all_2_20.txt";
		PipeRunner(input2, output2);
		
		String input3 = "C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/initial_posts.mallet";
		String output3 = "C:/Users/Chong/Google Drive/MOOC TM/result/PAM/Epidemic Forum/PAM_init_2_20.txt";
		PipeRunner(input3, output3);
		
		String input4 = "C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/posts_comments.mallet";
		String output4 = "C:/Users/Chong/Google Drive/MOOC TM/result/PAM/Epidemic Forum/PAM_all_2_20.txt";
		PipeRunner(input4, output4);
    }
	
	public static void PipeRunner(String input, String output)
	{
		try{
			System.out.println("START");
	        
			InstanceList instances = InstanceList.load (new File(input));  
	        
			int numSuperTopics = 2;
			int numSubTopics = 20;
			
			//int superTopics, int subTopics, double superTopicBalance, double subTopicBalance
	        HierarchicalPAM model = new HierarchicalPAM(numSuperTopics, numSubTopics, 1, 1);
	        
	        //InstanceList documents, InstanceList testing, int numIterations, int showTopicsInterval, int outputModelInterval, 
	        //int optimizeInterval, java.lang.String outputModelFilename, Randoms r
	        model.estimate(instances, instances, 1000, 20, 50, 10, "PAMmodel", new Randoms());
	        
	        String s=model.printTopWords(25, true);
	       
	        FileWriter fw = new FileWriter(output);
	        BufferedWriter bw = new BufferedWriter(fw);
	        
	        bw.write(s);
	        bw.close();
	        fw.close();
	 
	        System.out.println("Done");		
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
}
