package runMallet;

import java.io.File;
import java.io.IOException;

import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.InstanceList;

public class LDA
{

	public static void main(String[] args)
	{
		String input3 = "C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/initial_posts.mallet";
		String output3 = "C:/Users/Chong/Google Drive/MOOC TM/result/LDA/Epidemic Forum/LDA_init_20.txt";
		PipeRunner(input3, output3);
		
		String input4 = "C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/posts_comments.mallet";
		String output4 = "C:/Users/Chong/Google Drive/MOOC TM/result/LDA/Epidemic Forum/LDA_all_20.txt";
		PipeRunner(input4, output4);
	}
	
	public static void PipeRunner(String inputFile, String outputFile)
	{
		try
		{
			System.out.println("START");
	        
			InstanceList instances = InstanceList.load (new File(inputFile));  
	        
			int numTopics = 20;
			ParallelTopicModel model = new ParallelTopicModel(numTopics, numTopics, 0.01);
			
			//InstanceList training
			model.addInstances(instances);
			model.setNumThreads(4);
			model.setNumIterations(1000);

			model.estimate();
			model.printTopWords(new File(outputFile), 25, false);
			
			System.out.println("Done");
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
