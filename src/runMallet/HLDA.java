package runMallet;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import ModifiedMallet.HierarchicalLDA;
import cc.mallet.types.InstanceList;
import cc.mallet.util.Randoms;


public class HLDA
{

	public static void main(String[] args) throws FileNotFoundException, IOException
	{
		String input1 = "C:/Users/Chong/Google Drive/MOOC TM/data/Machine Learning Forum/initial_posts.mallet";
		String output1 = "C:/Users/Chong/Google Drive/MOOC TM/result/HLDA/Machine Learning Forum/HLDA_init.txt";
		PipeRunner(input1, output1);

		String input2 = "C:/Users/Chong/Google Drive/MOOC TM/data/Machine Learning Forum/posts_comments.mallet";
		String output2 = "C:/Users/Chong/Google Drive/MOOC TM/result/HLDA/Machine Learning Forum/HLDA_all.txt";
		PipeRunner(input2, output2);
		
		String input3 = "C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/initial_posts.mallet";
		String output3 = "C:/Users/Chong/Google Drive/MOOC TM/result/HLDA/Epidemic Forum/HLDA_init.txt";
		PipeRunner(input3, output3);
		
		String input4 = "C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/posts_comments.mallet";
		String output4 = "C:/Users/Chong/Google Drive/MOOC TM/result/HLDA/Epidemic Forum/HLDA_all.txt";
		PipeRunner(input4, output4);
	}
	
	public static void PipeRunner(String inputFile, String outputFile)
	{
		try{
			System.out.println("Start");
			
			InstanceList instances = InstanceList.load (new File(inputFile));  
	        
	        HierarchicalLDA model = new HierarchicalLDA();
	        
	        //(InstanceList instances, InstanceList testing, int numLevels, Randoms random) 
	        model.initialize(instances, instances, 3, new Randoms());
	        model.setProgressDisplay(true);
	        
	        int expectedNumTopics = 20;
	        model.setAlpha(50/expectedNumTopics);
	        
	        //numIterations
	        model.estimate(1000);
	
			model.printNodes(outputFile);
			
			System.out.println("Done");

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
