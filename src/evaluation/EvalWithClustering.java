package evaluation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class EvalWithClustering {

	ArrayList<Integer> trueValue;

	public static void main(String[] args) throws Exception {
		EvalWithClustering e = new EvalWithClustering();
		
		e.readTrueValue();
		
		BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/LDA.csv"));
		bw.append("Accurarcy");
		bw.append(",");
		bw.append("VariationalInformation");
		bw.append(",");
		bw.append("Jaccard Distance");
		bw.append(",");
		bw.append("purity");
		bw.append(",");
		bw.append("NMI");
		bw.newLine();
		
		for(int i = 8; i<=32; i+=8)
		{
			for(int k = 1; k<21; k++)
			{
				ArrayList<Integer> topicValue = new ArrayList<Integer>();
				String filePath = "C:/Users/Chong/Google Drive/MOOC TM/result/Key Word Extraction/LDA results tmp/CORLDA/"+String.valueOf(i)+"/"+String.valueOf(k);
				e.readInData(filePath,topicValue);
				
				bw.append(String.valueOf(e.F1(topicValue)));
				bw.append(",");
				bw.append(String.valueOf(e.VariationalInformation(topicValue)));
				bw.append(",");
				bw.append(String.valueOf(e.JaccardDistance(topicValue)));
				bw.append(",");
				bw.append(String.valueOf(e.purity(topicValue)));
				bw.append(",");
				bw.append(String.valueOf(e.NMI(topicValue)));
				bw.newLine();
			}
			bw.newLine();
			bw.newLine();
		}
		bw.close();
		
		System.out.println("done");
	}
	
	public EvalWithClustering()
	{
		trueValue = new ArrayList<Integer>();
	}
	
	public double F1(ArrayList<Integer> topicValue) throws Exception
	{
		if(trueValue.size()!=topicValue.size())
		{
			System.out.println("vector length not matched!");
			throw new Exception();
		}
		
		int TP = 0, FP = 0 , FN = 0, TN = 0;
		
		for(int i = 0; i<trueValue.size(); i++)
			for(int j = i+1; j<trueValue.size();j++)
				if(trueValue.get(i)==trueValue.get(j)) { //TP or FN
					if(topicValue.get(i)==topicValue.get(j))
						TP++;
					else
						FN++;
				}
				else { //FP or TN
					if(topicValue.get(i)==topicValue.get(j))
						FP++;
					else
						TN++;
				}
		
		
		double accuracy = (double)(TP)/(TP+FP);
		double f1 = (double)(2*TP)/(2*TP+FP+FN);
		
		return accuracy;
	}
	
	public double purity(ArrayList<Integer> topicValue) throws Exception
	{
		double p = 0;
		
		//cluster number, thread indices
		HashMap<Integer,HashSet<Integer>> trueSet = new HashMap<Integer,HashSet<Integer>>();
		HashMap<Integer,HashSet<Integer>> topicSet = new HashMap<Integer,HashSet<Integer>>();
		
		for (int i = 0; i<trueValue.size();i++)
		{
			if(trueSet.containsKey(trueValue.get(i)))
			{
				trueSet.get(trueValue.get(i)).add(i);
			}else
			{
				HashSet<Integer> threads = new HashSet<Integer>();
				threads.add(i);
				trueSet.put(trueValue.get(i), threads);
			}
		}
		
		for (int i = 0; i<topicValue.size();i++)
		{
			if(topicSet.containsKey(topicValue.get(i)))
			{
				topicSet.get(topicValue.get(i)).add(i);
			}else
			{
				HashSet<Integer> threads = new HashSet<Integer>();
				threads.add(i);
				topicSet.put(topicValue.get(i), threads);
			}
		}

		for(HashSet<Integer> topicS:topicSet.values())
		{
			ArrayList<Integer> tmpScores = new ArrayList<Integer>();
			for(HashSet<Integer> trueS:trueSet.values())
			{
				HashSet<Integer> tmp = new HashSet<Integer>();
				tmp.addAll(topicS);
				tmp.retainAll(trueS);
				tmpScores.add(tmp.size());
			}
			p+=Collections.max(tmpScores);
		}
		
		return (double)p/topicValue.size();
	}
	
	public double VariationalInformation(ArrayList<Integer> topicValue) throws Exception
	{
		
		if(trueValue.size()!=topicValue.size())
		{
			System.out.println("vector length not matched!");
			throw new Exception();
		}
		
		//Cluster index, documentIndexSet
		HashMap<Integer,HashSet<Integer>> topicClass = new HashMap<Integer,HashSet<Integer>>();
		HashMap<Integer,HashSet<Integer>> trueClass = new HashMap<Integer,HashSet<Integer>>();
		
		//transform the data into data structure
		int size = trueValue.size();
		for (int i = 0; i < size; i++)
		{
			int classIndex = trueValue.get(i);
			if(classIndex!=0)
			{
				if(trueClass.containsKey(classIndex))
				{
					trueClass.get(classIndex).add(i);
				}else{
					HashSet<Integer> s = new HashSet<Integer>();
					s.add(i);
					trueClass.put(classIndex, s);
				}
				
				classIndex = topicValue.get(i);
				if(topicClass.containsKey(classIndex))
				{
					topicClass.get(classIndex).add(i);
				}else{
					HashSet<Integer> s = new HashSet<Integer>();
					s.add(i);
					topicClass.put(classIndex, s);
				}	
			}
		}
		
		//calculate H(C), H(C')
		double HTopic = 0;
		for(HashSet<Integer> s:topicClass.values())
		{
			double p = (double)s.size()/size;
			HTopic -= p*log2(p);
		}
		
		double HTrue = 0;
		for(HashSet<Integer> s:trueClass.values())
		{
			double p = (double)s.size()/size;
			HTrue -= p*log2(p);
		}
		
		//calculate I(C,C')
		double I = 0;
		
		for( HashSet<Integer> s1:topicClass.values()){
			for( HashSet<Integer> s2:trueClass.values()){
				HashSet<Integer> tmp = new HashSet<Integer>(s1);
				tmp.retainAll(s2);
				double p = (double)tmp.size()/size;
				double p1 = (double)s1.size()/size;
				double p2 = (double)s2.size()/size;
				I += p*log2(p/(p1*p2));
				
			}
		}
		
		double VI = HTopic+HTrue-2*I;
		return VI;
	}
	
	public double NMI(ArrayList<Integer> topicValue) throws Exception
	{
		
		if(trueValue.size()!=topicValue.size())
		{
			System.out.println("vector length not matched!");
			throw new Exception();
		}
		
		//Cluster index, documentIndexSet
		HashMap<Integer,HashSet<Integer>> topicClass = new HashMap<Integer,HashSet<Integer>>();
		HashMap<Integer,HashSet<Integer>> trueClass = new HashMap<Integer,HashSet<Integer>>();
		
		//transform the data into data structure
		int size = trueValue.size();
		for (int i = 0; i < size; i++)
		{
			int classIndex = trueValue.get(i);
			if(classIndex!=0)
			{
				if(trueClass.containsKey(classIndex))
				{
					trueClass.get(classIndex).add(i);
				}else{
					HashSet<Integer> s = new HashSet<Integer>();
					s.add(i);
					trueClass.put(classIndex, s);
				}
				
				classIndex = topicValue.get(i);
				if(topicClass.containsKey(classIndex))
				{
					topicClass.get(classIndex).add(i);
				}else{
					HashSet<Integer> s = new HashSet<Integer>();
					s.add(i);
					topicClass.put(classIndex, s);
				}	
			}
		}
		
		//calculate H(C), H(C')
		double HTopic = 0;
		for(HashSet<Integer> s:topicClass.values())
		{
			double p = (double)s.size()/size;
			HTopic -= p*log2(p);
		}
		
		double HTrue = 0;
		for(HashSet<Integer> s:trueClass.values())
		{
			double p = (double)s.size()/size;
			HTrue -= p*log2(p);
		}
		
		//calculate I(C,C')
		double I = 0;
		
		for( HashSet<Integer> s1:topicClass.values()){
			for( HashSet<Integer> s2:trueClass.values()){
				HashSet<Integer> tmp = new HashSet<Integer>(s1);
				tmp.retainAll(s2);
				double p = (double)tmp.size()/size;
				double p1 = (double)s1.size()/size;
				double p2 = (double)s2.size()/size;
				I += p*log2(p/(p1*p2));
				
			}
		}
		
		double NMI = 2*I/(HTopic+HTrue);
		return NMI;
	}
	
	public double log2(double a)
	{
		//if zero
		if (new Double(0.0).equals(a))
			return 0.0;
		return Math.log(a)/Math.log(2);
	}
	
	public double JaccardDistance(ArrayList<Integer> topicValue) {
		double JScore = 0;
		
		//cluster number, thread indices
		HashMap<Integer,HashSet<Integer>> trueSet = new HashMap<Integer,HashSet<Integer>>();
		HashMap<Integer,HashSet<Integer>> topicSet = new HashMap<Integer,HashSet<Integer>>();
		
		for (int i = 0; i<trueValue.size();i++)
		{
			if(trueSet.containsKey(trueValue.get(i)))
			{
				trueSet.get(trueValue.get(i)).add(i);
			}else
			{
				HashSet<Integer> threads = new HashSet<Integer>();
				threads.add(i);
				trueSet.put(trueValue.get(i), threads);
			}
		}
		
		for (int i = 0; i<topicValue.size();i++)
		{
			if(topicSet.containsKey(topicValue.get(i)))
			{
				topicSet.get(topicValue.get(i)).add(i);
			}else
			{
				HashSet<Integer> threads = new HashSet<Integer>();
				threads.add(i);
				topicSet.put(topicValue.get(i), threads);
			}
		}

		for(HashSet<Integer> topicS:topicSet.values())
		{
			ArrayList<Double> tmpScores = new ArrayList<Double>();
			for(HashSet<Integer> trueS:trueSet.values())
			{
				tmpScores.add(JaccardIndex(topicS,trueS));
			}
			//System.out.println(tmpScores.toString());
			JScore+=Collections.max(tmpScores);
		}
		
		return JScore/topicSet.size();
	}
	
	public double JaccardIndex(HashSet<Integer> A,HashSet<Integer> B)
	{
		double JI = 0;
		
		HashSet<Integer> intersect = new HashSet<Integer>();
		HashSet<Integer> union = new HashSet<Integer>();
		
		intersect.addAll(A);
		intersect.retainAll(B);
		
		union.addAll(A);
		union.addAll(B);
		
		JI = (double)intersect.size()/union.size();
		
		return JI;
	}

	
	
	public void readInData(String filePath,ArrayList<Integer> topicValue) throws IOException
	{
		BufferedReader br1 = new BufferedReader(new FileReader(filePath));
		
		String line1 = "";
		while((line1 = br1.readLine())!=null)
		{
			topicValue.add(Integer.valueOf(line1.trim()));
		}
	
		br1.close();
	}
	
	public void readTrueValue() throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader("C:/Users/Chong/Google Drive/MOOC TM/result/Key Word Extraction/true class.txt"));
		
		String line = "";
		while((line = br.readLine())!=null)
		{
			trueValue.add(Integer.valueOf(line.trim()));
		}
		br.close();
	}

}
