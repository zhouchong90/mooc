package evaluation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class TopicCoherence {
	
	final int minWordLength = 2;

	public HashSet<String> stopList = new HashSet<String>();
	public HashMap<Integer,String> trueClass = new HashMap<Integer,String>();
	/**
	 * term-index, index-term dictionary
	 */
	public HashMap<String, Integer> dictionary = new HashMap<String, Integer>();
	public HashMap<Integer, String> inverseDictionary = new HashMap<Integer, String>();

	public String fileFolder = "C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/posts_comments_lemmatized";

	// term-document frequency matrix
	public int[][] termDocMat;

	public int documentSize = 0;
	
	// sum of matrix
	int matrixSum = 0;
	int[] termSums;
	int[] docSums;
		
	public static void main(String[] args) throws IOException {
		TopicCoherence t = new TopicCoherence();
		
		double score = t.coherenceScore("C:/Users/Chong/Desktop/Evaluation Docs/Mooc/DF_LDA8.txt");
	
		System.out.println("topic Coherence:" + score);
	
	}
	
	public TopicCoherence()
	{
		readInStopList();
		readInValidDocList();
		processDictionary();
		init();
		readInData();
		calSum();
	}
	
	public void readInStopList() {
		try {
			BufferedReader br = new BufferedReader(
					new FileReader(
							"C:/Users/Chong/Google Drive/MOOC TM/code/data cleaning/stopWords.txt"));

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.isEmpty())
					stopList.add(line.trim());
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readInValidDocList()
	{
		try {
			BufferedReader br = new BufferedReader(
					new FileReader(
							"C:/Users/Chong/Google Drive/MOOC TM/Syllabus TM paper/True Class.csv"));

			String line;
			while ((line = br.readLine()) != null) {
				if (!line.isEmpty())
				{
					String[] s = line.split(",");
					if(!s[1].equals("0"))
						trueClass.put(Integer.parseInt(s[0].trim()), s[1].trim() );
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void processDictionary() {
		try {
			File fileFolder = new File(this.fileFolder);
			File[] listOfFiles = fileFolder.listFiles();
			int index = 0;

			for (File doc : listOfFiles)
				
				
				if (doc.isFile() && doc.getName().split("\\.")[1].equals("txt") && trueClass.containsKey(Integer.parseInt(doc.getName().split("\\.")[0]))) {
					BufferedReader br = new BufferedReader(new FileReader(doc));

					String line;
					while ((line = br.readLine()) != null) {
						String[] words = line.split(" ");
						for (String word : words)
							if (word.length() >= minWordLength)
								if (!dictionary.containsKey(word)
										&& !stopList.contains(word)) {
									dictionary.put(word, index);
									inverseDictionary.put(index, word);
									index++;
								}
					}
					br.close();
					documentSize++;
				}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void init() {
		termDocMat = new int[dictionary.size()][documentSize];

		termSums = new int[termDocMat.length];
		docSums = new int[termDocMat[0].length];
	}
	
	public void readInData() {
		try {
			File fileFolder = new File(this.fileFolder);
			File[] listOfFiles = fileFolder.listFiles();

			int docIndex = 0;
			for (File doc : listOfFiles)
				
				if (doc.isFile() && doc.getName().split("\\.")[1].equals("txt")&& trueClass.containsKey(Integer.parseInt(doc.getName().split("\\.")[0]))) {
					BufferedReader br = new BufferedReader(new FileReader(doc));

					String line;
					while ((line = br.readLine()) != null) {
						String[] words = line.split(" ");
						for (String word : words) {
							if (dictionary.containsKey(word))
								termDocMat[dictionary.get(word)][docIndex]=1;
						}
					}
					br.close();
					docIndex++;
				}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void calSum() {
		for (int i = 0; i < termDocMat.length; i++)
			for (int j = 0; j < termDocMat[0].length; j++)
				matrixSum += termDocMat[i][j];

		// calculate the rowsum and colsum
		for (int i = 0; i < termDocMat.length; i++) {
			for (int j = 0; j < termDocMat[0].length; j++) {
				termSums[i] += termDocMat[i][j];
				docSums[j] += termDocMat[i][j];
			}
		}
	}
	
	public double coherenceScore(String filePath) throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		
		int numTopics = 0;
		
		String line;
		
		double score = 0;
		
		while ((line = br.readLine()) != null) {
			
			numTopics++;
		
			String[] words = line.split(",");
			ArrayList<String> topics = new ArrayList<String>();
			
			for (String word : words) {
				topics.add(word);
			}
			
			for (int m = 1; m<topics.size(); m++)
			{
				for(int l = 0; l<m-1; l++)
				{
					score += calCoherence(topics.get(m),topics.get(l));
				}
			}
		}
		
		br.close();
		
		return score/numTopics;

	}
	
	private double calCoherence(String m, String l) {

		int mIndex = dictionary.get(m);
		int lIndex = dictionary.get(l);
		
		double d2 = 1;
		for(int i = 0; i<documentSize; i++)
		{
			if(termDocMat[mIndex][i]>0 && termDocMat[lIndex][i]>0)
			{
				d2++;
			}
		}
		
		return log2(d2/termSums[lIndex]);
	}

	public double log2(double a)
	{
		//if zero
		if (new Double(0.0).equals(a))
			return 0.0;
		return Math.log(a)/Math.log(2);
	}
}
