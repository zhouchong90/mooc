package evaluation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

public class ExtractTrueClassValue {

	HashMap<String, String> fileClass;
	HashSet<String> fileNames;
	
	public static void main(String[] args) throws IOException {
		
		
		ExtractTrueClassValue e = new ExtractTrueClassValue();
		
		e.readInFileNames();
		
		
		e.run();
		e.printClassToFile();
	}

	public ExtractTrueClassValue()
	{
		fileClass = new HashMap<String,String>();
		fileNames = new HashSet<String>();
	}

	private void readInFileNames() {
		
		File fileFolder = new File(
				"C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/posts_comments_lemmatized/");
		File[] listOfFiles = fileFolder.listFiles();
		for (File file : listOfFiles) {
			if (file.isFile() && file.getName().split("\\.")[1].equals("txt"))
			{
				String fileName = file.getName().split("\\.")[0];
				fileNames.add(fileName);
			}
		}
		
	}
	
	private void run() throws IOException {
		// for all files in the folder
		File fileFolder = new File(
				"C:/Users/Chong/Google Drive/MOOC TM/data/Epidemic Forum/data_raw/");
		File[] listOfFiles = fileFolder.listFiles();

		int j = 0;// total post count
		int documentSize = 0;
		// for every document in the folder
		for (File file : listOfFiles) {
			if (file.isFile() && file.getName().split("\\.")[1].equals("json")) {
				String name = file.getName().trim();
				String[] names = StringUtils.split(name, ".|_");
				String fileName = names[0];
				
				if(fileNames.contains(fileName))
				{
					String fileContent = new String(Files.readAllBytes(Paths
							.get(file.getPath())), StandardCharsets.UTF_8);
					fileContent = StringUtils.stripAccents(fileContent);
					fileContent = fileContent.replaceAll("[^\\p{ASCII}]", " ");
					fileContent = fileContent.replaceAll("\n", " ");
					fileContent = fileContent.replaceAll("nbsp", " ");
	
					// parse the document
					try {
						JsonReader reader = new JsonReader(new StringReader(
								fileContent));
						reader.setLenient(true);
						JsonParser parser = new JsonParser();
						JsonObject document = parser.parse(reader)
								.getAsJsonObject();
	
						// extract crumbs
						try {
							JsonArray crumbs = document.getAsJsonArray("crumbs");
							
							String fClass = "";
							for (int i = 0; i < crumbs.size(); i++) {
								JsonObject post = crumbs.get(i).getAsJsonObject();
								String text = post.get("title").getAsString();
								
								Pattern p = Pattern.compile("Week \\d");
								Matcher m = p.matcher(text);
								if(m.find())
								{
									fClass = m.group();
								}
							}
							
							if(fClass.isEmpty())
							{
								fileClass.put(fileName, new String("0"));
							}
							else
							{
								fileClass.put(fileName, fClass);
							}
							
						} catch (Exception e) {
							// System.err.println(e);
						}
						documentSize++;
					} catch (Exception e) {
						System.err.println(file.getName()
								+ " Fatal error, dumping this file");
						System.err.print(e);
					}
				}
			}
		}
		System.out.println("document size:" + documentSize);
	}

	private void printClassToFile() throws IOException {
		
		BufferedWriter bw = new BufferedWriter(new FileWriter("C:/Users/Chong/Desktop/True Class.csv"));
		
		for (String s:fileClass.keySet())
		{
			bw.write(String.valueOf(s));
			bw.write(",");
			bw.write(String.valueOf(fileClass.get(s)));
			bw.newLine();
		}
		bw.close();
	}
}
